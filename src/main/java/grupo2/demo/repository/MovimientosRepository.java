package grupo2.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import grupo2.demo.entity.Movimientos;

@Repository
public interface MovimientosRepository extends JpaRepository<Movimientos, Long>{


	//@Query(value="SELECT m FROM Movimientos JOIN m.cuentaBancaria cb WHERE cb.id=:id")
	//@Query(value="SELECT m FROM Movimientos JOIN m.cuentaBancaria cb WHERE cb.id=:id")
	//List<Movimientos> consultaMovimientosDni(@Param("id") List<CuentaBancariaRepository> id);

}

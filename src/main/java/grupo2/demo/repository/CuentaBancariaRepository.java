package grupo2.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.*;

import grupo2.demo.entity.CuentaBancaria;

@Repository
public interface CuentaBancariaRepository extends JpaRepository<CuentaBancaria, Long>{

	@Query(value="SELECT c FROM CuentaBancaria c JOIN c.cliente cl WHERE cl.dni = :dni")
	List<CuentaBancaria> search(@Param("dni") String dni);

}

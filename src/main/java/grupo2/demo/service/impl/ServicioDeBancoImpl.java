package grupo2.demo.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import grupo2.demo.entity.Cliente;
import grupo2.demo.entity.CuentaBancaria;
import grupo2.demo.entity.Movimientos;
import grupo2.demo.repository.ClienteRepository;
import grupo2.demo.repository.CuentaBancariaRepository;
import grupo2.demo.repository.MovimientosRepository;
import grupo2.demo.service.ServicioDeBanco;

@Service
public class ServicioDeBancoImpl implements ServicioDeBanco{
	@Autowired
	private ClienteRepository clienteRepo;
	@Autowired
	private CuentaBancariaRepository cuentaRepo;
	@Autowired
	private MovimientosRepository movirepo;

	@Override
	public Cliente saveCliente(Cliente cliente) {
		// TODO Auto-generated method stub
		clienteRepo.save(cliente);
		return null;
	}

	@Override
	public CuentaBancaria saveCuenta(CuentaBancaria cuenta) {
		// TODO Auto-generated method stub
		cuentaRepo.save(cuenta);
		return null;
	}

	@Override
	public Cliente modifyCliente(Cliente cliente) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CuentaBancaria modifyCuenta(CuentaBancaria cuenta) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Movimientos addMovimiento(Movimientos movimiento) {
		// TODO Auto-generated method stub
		movirepo.save(movimiento);
		return null;
	}

	@Override
	public List<CuentaBancaria> showCuentaByDni(String dni) {
		// TODO Auto-generated method stub
		List<CuentaBancaria> lista = cuentaRepo.search(dni);
		return lista;
		
	}

	@Override
	public List<Movimientos> showMovimientosByDni(String dni) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean deposito(CuentaBancaria cuenta, double dinero) {
		// TODO Auto-generated method stub
		cuenta.setSaldo(cuenta.getSaldo() + dinero);
		cuentaRepo.save(cuenta);
		return true;
	}
	@Override
	public boolean extraccionCuenta(CuentaBancaria cuenta, double montoExtraccion) {
		// TODO Auto-generated method stub
		if (cuenta.getSaldo()>=montoExtraccion) {
			cuenta.setSaldo(cuenta.getSaldo()-montoExtraccion);
			cuentaRepo.save(cuenta);
			return true;			
		}else {
			return false;
		}		

	}
	
	
	
}

package grupo2.demo.service;

import java.util.List;
import grupo2.demo.entity.Cliente;
import grupo2.demo.entity.CuentaBancaria;
import grupo2.demo.entity.Movimientos;

public interface ServicioDeBanco {
	public boolean deposito(CuentaBancaria cuenta,double i);
	
	public Cliente saveCliente(Cliente cliente);
	
	public CuentaBancaria saveCuenta(CuentaBancaria cuenta);
	
	public Cliente modifyCliente(Cliente cliente);
	
	public CuentaBancaria modifyCuenta(CuentaBancaria cuenta);
	
	public Movimientos addMovimiento(Movimientos movimiento); 
	
	public List<CuentaBancaria> showCuentaByDni(String dni);
	
	public List<Movimientos> showMovimientosByDni(String dni);
	
	public boolean extraccionCuenta(CuentaBancaria cuenta, double montoExtraccion);

}

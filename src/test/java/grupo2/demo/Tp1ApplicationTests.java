package grupo2.demo;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import grupo2.demo.entity.Cliente;
import grupo2.demo.entity.CuentaBancaria;
import grupo2.demo.entity.Movimientos;
import grupo2.demo.service.ServicioDeBanco;

@SpringBootTest
class Tp1ApplicationTests {
	@Autowired
	ServicioDeBanco servicio;
	
	@Test
	void contextLoads() {
		Cliente cli = new Cliente();
		cli.setDni("45475");
		cli.setDomicilio("Av Ricardo Balvin 4555");
		cli.setEmail("jfasdoar1040@gmail.com");
		cli.setNombre("carlos");
		servicio.saveCliente(cli);
		
		CuentaBancaria cu = new CuentaBancaria();
		CuentaBancaria cu2 = new CuentaBancaria();
		cu.setCliente(cli);
		cu.setSaldo(1000);
		
		cu2.setCliente(cli);
		cu2.setSaldo(1000);
		
		servicio.saveCuenta(cu);
		servicio.saveCuenta(cu2);
		
		servicio.extraccionCuenta(cu, 600);
		
		servicio.deposito(cu, 2000);
		

		//Movimientos mo = new Movimientos();
		//mo.setCuenta(cu);

		//servicio.addMovimiento(mo);
		
		List<CuentaBancaria> lita =servicio.showCuentaByDni("45475");
		
		System.out.println("Iniciando");
		
		for (CuentaBancaria num : lita) 
        {
			System.out.println("holas");
			System.out.println(num);
        } 

		

		
	}

}
